package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        /**Checking the null List*/
        if(x == null || y == null){
            throw new IllegalArgumentException();
        }
        /**Checking empty List*/
        if(x.isEmpty() && y.isEmpty()){
            return true;
        }else {if(y.isEmpty()){
            System.out.println(false + " empty one");
            return false;
        }}
        /**Comparing 2 List by using third*/
        List <Object> Z = new ArrayList();
        int startj = 0;
        for (int i = 0; i < x.size(); i++) {
            Object elemX = x.get(i);
            for (int j = startj; j < y.size(); j++) {
                if (elemX.equals(y.get(j)) && !Z.contains(y.get(j))) {
                    System.out.println(elemX + " "+ i + " " + y.get(j) + " " + y.indexOf(elemX));
                    Z.add(y.get(j));
                    if(startj <= j) {
                        System.out.println(startj);
                        startj = j;
                    }else {
                        System.out.println(false + " with startj");
                        return false;
                    }
                }
            }
        }
        /**Checking the similarity of sizes X and Z Lists*/
        if(x.size() != Z.size()){
            System.out.println(false + " different sizes");
            return false;
        }
        /**Checking the similarity of sizes X and Z Lists*/
        for (int j = 0; j < x.size(); j++) {
            if(x.get(j) != Z.get(j)){
                System.out.println(false + " different order");
                return false;
            }
        }
        return true;
    }
}
