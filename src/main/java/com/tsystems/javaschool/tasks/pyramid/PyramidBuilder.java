package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {
private static int position = 0;
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        /* Checking for exceptions*/
        try {
            Collections.sort(inputNumbers);
        }catch (NullPointerException e){
            throw new CannotBuildPyramidException("Null in the list of Integer");
        }catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException("Some others exception");
        }
        /* Checking of pyramid symetry*/
        int pyramidLevel = 0;
        if(inputNumbers.size() > 0) {
            int lengthOfList = inputNumbers.size();
            while (lengthOfList > 0) {
                pyramidLevel++;
                lengthOfList -= pyramidLevel;
            }
            if (lengthOfList != 0) {
                throw new CannotBuildPyramidException("Non-symetrical pyramid");
            }
        }
        /* Converting Lists to Array*/
        int[][] pyramid = new int[pyramidLevel][pyramidLevel*2-1];
        for (int i = 0; i < pyramidLevel; i++) {
            List<Integer> list = fillTheResultArray(inputNumbers,pyramidLevel,i);
            System.out.println(list);
            for (int j = 0; j < pyramidLevel*2-1; j++) {
                pyramid[i][j] = list.get(j);
            }
        }
        return pyramid;
    }
    /* Fill the list for every level of pyramid starting from the top*/
    public static List<Integer> fillTheResultArray(List<Integer> sortedInputNumbers, int pyramidlevel, int currentPyramidLevel){
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = currentPyramidLevel; i < pyramidlevel-1; i++) {
            result.add(0);
        }
        int start = position;
        for (int i = start; i <(start+currentPyramidLevel+1); i++) {
            result.add(sortedInputNumbers.get(i));
            position++;
            result.add(0);
        }
        result.remove(result.size()-1);
        for (int i = currentPyramidLevel; i < pyramidlevel-1; i++) {
            result.add(0);
        }
        return result;
    }
}
