package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public abstract class AbstractCalculator {

    public String evaluateStatement(String statement) {
        Stack<String> stackOfDigit = new Stack<>();
        Stack<Character> stackOfAction = new Stack<>();
        int priority;
        for (int i = 0; i < statement.length(); i++) {
            priority = getPriority(statement.charAt(i));
            if (priority == 1) {
                String nextInt = getNext(statement,i);
                stackOfDigit.push(nextInt);
                i+= nextInt.length()-1;
            }
            if (priority == 2) {
                stackOfAction.push(statement.charAt(i));
            }
            if(priority > 2){
                if (stackOfAction.isEmpty()){
                    stackOfAction.push(statement.charAt(i));
                } else {
                    if (getPriority(stackOfAction.peek()) < getPriority(statement.charAt(i))) {
                        stackOfAction.push(statement.charAt(i));
                    } else {
                        if(getPriority(stackOfAction.peek()) >= getPriority(statement.charAt(i)) || stackOfAction.isEmpty()) {
                            stackOfDigit.push(calculateStack(stackOfAction.pop(), stackOfDigit.pop(), stackOfDigit.pop()));
                            stackOfAction.push(statement.charAt(i));
                        }
                    }
                }
            }
            if(priority == 0){
                while (true){
                    System.out.println("Im doing with " + stackOfAction.peek());
                    stackOfDigit.push(calculateStack(stackOfAction.pop(), stackOfDigit.pop(), stackOfDigit.pop()));
                    stackOfAction.pop();
                    if(stackOfAction.isEmpty()){
                        break;
                    }
                    if(stackOfAction.peek() != '('){
                        break;
                    }
                }
            }
            System.out.println("Action " + stackOfAction);
            System.out.println("Digit " + stackOfDigit);
            if(i == statement.length()-1) {
                while (!stackOfAction.isEmpty()) {
                    stackOfDigit.push(calculateStack(stackOfAction.pop(), stackOfDigit.pop(), stackOfDigit.pop()));
                }
            }
        }
        System.out.println(stackOfDigit.peek());
        return stackOfDigit.peek();
    }

    public String calculateStack (Character action, String firstOperand, String secondOperand) {
        if (action == '+') {
            System.out.println(Integer.valueOf(firstOperand) + Integer.valueOf(secondOperand) + "");
            return (Integer.valueOf(firstOperand) + Integer.valueOf(secondOperand) + "");
        }
        if (action == '-') {
            System.out.println(Integer.valueOf(firstOperand) - Integer.valueOf(secondOperand));
            return (Integer.valueOf(secondOperand) - Integer.valueOf(firstOperand) + "");
        }
        if (action == '*') {
            System.out.println(Integer.valueOf(firstOperand) * Integer.valueOf(secondOperand) + "");
            return (Integer.valueOf(firstOperand) * Integer.valueOf(secondOperand) + "");
        }
        if (action == '/') {
            System.out.println(Integer.valueOf(secondOperand) / Integer.valueOf(firstOperand) + "");
            return (Integer.valueOf(secondOperand) / Integer.valueOf(firstOperand) + "");
        }
        return "error";
    }

    public String getNext (String statement, int i){
        String result = "";
        char c;
        for (int j = i; j < statement.length(); j++) {
            c = statement.charAt(j);
            if (c != '/' && c != '*' && c != '+' && c != '-' && c != '(' && c != ')'){
                result += c;
            }else {return result;}
        }
        return result;
    }

    public int getPriority (Character currentChar){
        if(currentChar == '*' || currentChar == '/'){ return 4;}
        else{if(currentChar == '+' || currentChar == '-') { return 3;}
        else{if(currentChar == '('){return 2;}
        else{if(currentChar == ')'){return 0;}
        else{if(Character.isDigit(currentChar) || currentChar == '.'){
            return 1; }}}
            return -2;}}}
}

