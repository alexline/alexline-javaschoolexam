package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Arrays;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            statement = pairOfParentheses(statement);
        } catch (NullPointerException e) {
            return null;
        }

        if(statement == null || statement.indexOf(' ') != -1 || statement.equals("")){
            return null;
        }

        for (int i = 0; i < statement.length()-1; i++) {
            char current = statement.charAt(i);
            char next = statement.charAt(i+1);
            ArrayList<Character> actions = new ArrayList<>(Arrays.asList('+','-','*','/'));
            if(actions.contains(current) && actions.contains(next)){
                return null;
            }
        }

        if(statement.indexOf('.') != -1){
            DobleCalculator calculator = new DobleCalculator();
            try {
                return calculator.evaluateStatement(statement);
            } catch (NumberFormatException e){
                return null;
            }catch (ArithmeticException e) {
                return null;
            }
        } else {
            IntCalculator calculator = new IntCalculator();
            try {
                return calculator.evaluateStatement(statement);
            } catch (NumberFormatException e){
                return null;
            }catch (ArithmeticException e) {
                return null;
            }
        }
    }
    public String pairOfParentheses (String statement){
        int counterIn = 0;
        int counterOut = 0;
        for (int i = 0; i < statement.length(); i++) {
            if(statement.charAt(i) == '('){
                counterIn++;
            }
            if(statement.charAt(i) == ')'){
                counterOut++;
            }
        }
        if(counterIn == counterOut){
            return statement;
        }
        return null;
    }
}
